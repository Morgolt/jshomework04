/*

1. Описати своїми словами навіщо потрібні функції у програмуванні.

Часто нам треба повторювати одну й ту саму дію в багатьох частинах програми. 
Щоб не повторювати той самий код у багатьох місцях, придумані функції. Функції є основними "будівельними блоками" програми.

2. Описати своїми словами, навіщо у функцію передавати аргумент.

Аргумент – це значення, яке передається функції під час її виклику.

Ми оголошуємо функції зі списком параметрів, потім їх викликаємо, передаючи аргументи.

3. Що таке оператор return та як він працює всередині функції?

Функція може повернути результат, який буде переданий у код, що її викликав.

Директива return може знаходитись у будь-якому місці тіла функції. Як тільки виконання доходить до цього місця, 
функція зупиняється, і значення повертається в код, що її викликав.


*/

let userNumberOne = '';

while (!/^[0-9]+$/.test(userNumberOne)) {
  userNumberOne = parseInt(prompt('Будь-ласка, введіть перше ціле число /цифри 0-9 дозволено/)', userNumberOne), 10);
}

let userNumberTwo = '';

while (!/^[0-9]+$/.test(userNumberTwo)) {
  userNumberTwo = parseInt(prompt('Будь-ласка, введіть друге ціле число /цифри 0-9 дозволено/)', userNumberTwo), 10);
}

let userСount = '';

do {
  userСount = prompt('A теперь, введіть знак арифметичної операції /+, -, *, / /)', userСount);
} while (!(userСount === '+' || userСount === '-' || userСount === '*' || userСount === '/'));

const calc = (userNumberOne, userNumberTwo, userCount) => {
  switch (userCount) {
    case '+':
      return userNumberOne + userNumberTwo;
    case '-':
      return userNumberOne - userNumberTwo;
    case '/':
      if (userNumberTwo) {
        return userNumberOne / userNumberTwo;
      } else alert('Ділити на нуль не можна');

    case '*':
      return userNumberOne * userNumberTwo;
  }
};

console.log(userNumberOne + ' ' + userСount + ' ' + userNumberTwo + ' = ' + calc(userNumberOne, userNumberTwo, userСount));
